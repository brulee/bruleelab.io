# Consent Etiquette

_This file is Official Brulee Etiquette & Protocol_

\[\[_TOC_]]

It is everyone’s responsibility to understand, ask for, and choose whether to give or withhold consent. Together, we seek to create a supportive and consensual space where people are accepted and able to explore themselves and their sexuality. We expect everyone at Brulee events to follow these guidelines while you are with us. (We hope you will follow them outside our doors as well.)

In concept, each of these items are enforceable and policeable. If you violate them, everyone in the community is authorized to report them.

#### No Touching

No touching people or personal property without permission.

#### Equality

Treat everyone as an equal by default. Everyone has responsibility to obtain, provide, or withhold consent regardless of sex, gender, race, ethnicity, ability, age, orientation, relationship status, sexual power dynamics, or any other identity.

#### Capacity

Each participant is responsible for making sure, to the best of their ability, that everyone involved has the physical, mental, and emotional capacity to give informed and voluntary consent during negotiation and the activity itself.

#### Withdrawing Consent

Anyone can withdraw consent at any time during any activity. A 'no' in any form, verbal or nonverbal, should be taken as a withdrawal of consent. All participants are responsible for immediately stopping any activity at the moment consent has been withdrawn.

#### Negotiation

Negotiate anything beyond simple activities. Should sex, sexual contact, or sexual play be involved: Give clear and explicit consent to the negotiated acts.

#### Safe Words

Participants need to agree on meanings for safe words or safe signs prior to engaging in sexual and/or BDSM activities. The house will uphold standard "Green," "Yellow," "Red" safe word designations.

* "Green" means "Go!": All agreed upon activities may commence. Have fun!
* "Yellow" means "Slow down!": The activity as a whole may be acceptable but a certain act may be pushing a boundary or vice versa. Or the rate at which an activity has accelerated is beyond a participant's level of comfort. Cease the activity to discuss why "Yellow" was called and determine how and if activities shall recommence.
* "Red" means "Stop!": All engaged in activities must cease immediately! This is non-negotiable as it could cause permanent harm or injury to an engaged party if the activity were to continue. It is up to the participants to determine if, when, and how to discuss the circumstances that lead to "Red" being called. Notify a Consent Counselor as soon after the incident as possible so we may take a documented report and provide resources as needed or requested.

#### Renegotiation

Unless previously agreed upon, we recommend avoiding re-negotiating in the middle of an activity. When a person is not in a clear state of mind, you may not have full or informed consent even though they agree in the heat of the moment. ​​

### Consent is...

#### Essential

#### Specific

#### Active

#### Vocal

#### Continual

#### Retractable

#### Renegotiable

#### Fun

#### Enthusiastic

#### Comfortable

#### Sober

#### Conscious

#### Clear

#### Voluntary

#### Willing

#### Informed
