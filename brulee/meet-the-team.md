---
cover: >-
  https://images.unsplash.com/photo-1519389950473-47ba0277781c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2970&q=80
coverY: 0
---

# Meet the Team!

## C-3PO

👋 Consent Etiquette & Protocol Officer — 💌 c3po@brulee.co — 🇺🇸 Monument (GMT-6)

![](<../.gitbook/assets/98FA87A0-FFD3-4C46-97C4-61CFD739C09C\_1\_102\_o (1).jpeg>)

### Bio

## Rima Paterson

👋 CTO — 💌 rima@company.com — 🇳🇱 Amsterdam (GMT+1)

![](https://images.unsplash.com/photo-1502764613149-7f1d229e230f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8\&ixlib=rb-1.2.1\&auto=format\&fit=crop\&w=2972\&q=80)

### Bio

## Stefan Barr

👋 Head of Product — 💌 stefan@company.com — 🇫🇷 Marseille (GMT+1)

![](https://images.unsplash.com/photo-1601935111741-ae98b2b230b0?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8\&ixlib=rb-1.2.1\&auto=format\&fit=crop\&w=2970\&q=80)

### Bio
