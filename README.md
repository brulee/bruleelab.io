---
coverY: 0
config.style.googleFont: '<link href="https://fonts.googleapis.com/css?family=Aladin" rel="stylesheet">'
config.style.page.font: 'Aladin 18'
---

# Master Consent Protocol

## Welcome aboard!

Welcome to the company wiki! Here you'll find everything you need to know about the company.
