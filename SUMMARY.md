# Table of contents

* [Master Consent Protocol](README.md)

## BRÛLÉE

* [Vision and Values](brulee/vision-and-values.md)
* [Meet the Team!](brulee/meet-the-team.md)

## Etiquette

* [Consent Etiquette](etiquette/consent-etiquette.md)

## PROTOCOL

* [Consent Incident Report](protocol/consent-incident-report.md)

***

* [CIR](cir.md)
